#include <AFMotor.h>

AF_DCMotor motorA(1); // Silnik A jest podłączony do złącza numer 1
AF_DCMotor motorB(2); // Silnik B jest podłączony do złącza numer 2

void setup() {
  Serial.begin(9600);           // ustawienie biblioteki Serial na prędkość 9600 bps
  Serial.println("Motor test!");

  // ustawienie prędkości obrotowej silników
  motorA.setSpeed(200);
  motorB.setSpeed(200);
  
  // zatrzymanie silników
  motorA.run(RELEASE);
  motorB.run(RELEASE);
}

void loop() {
  uint8_t i;

  // Sterowanie silnikiem A w przód, a silnikiem B w tył
  motorA.run(FORWARD);
  motorB.run(BACKWARD);
  
  for (i=0; i<255; i++) {
    motorA.setSpeed(i);  
    motorB.setSpeed(i);
    delay(3);
  }
 
  for (i=255; i!=0; i--) {
    motorA.setSpeed(i);  
    motorB.setSpeed(i);
    delay(10);
  }
  
  // Zatrzymanie silników
  motorA.run(RELEASE);
  motorB.run(RELEASE);
  delay(1000);
  
  // Sterowanie silnikAMI
  motorA.run(FORWARD);
  motorB.run(FORWARD);
  
  for (i=0; i<255; i++) {
    motorA.setSpeed(i);  
    motorB.setSpeed(i);
    delay(10);
  }
 
  for (i=255; i!=0; i--) {
    motorA.setSpeed(i);  
    motorB.setSpeed(i);
    delay(10);
  }

  // Zatrzymanie silników
  motorA.run(RELEASE);
  motorB.run(RELEASE);
  delay(1000);
}
