const int leftSensorPin = A0;
const int rightSensorPin = A1;
const int middleSensorPin = A2;

void setup() {
  pinMode(leftSensorPin, INPUT);
  pinMode(rightSensorPin, INPUT);
  pinMode(middleSensorPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  int leftSensorValue = digitalRead(leftSensorPin);
  int rightSensorValue = digitalRead(rightSensorPin);
  int middleSensorValue = digitalRead(middleSensorPin);
  Serial.print("Left Sensor: ");
  Serial.print(leftSensorValue);
  Serial.print("   Middle Sensor: ");
  Serial.println(middleSensorValue);
  Serial.print("   Right Sensor: ");
  Serial.println(rightSensorValue);
  delay(500);
}
